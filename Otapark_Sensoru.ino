const int trig = 11;
const int echo = 12;

const int yled =  2;
const int sled =  3;
const int kled =  4;

const int buzzer = 5;

int sure;
int mesafe;


void setup()
{
  pinMode(yled , OUTPUT);
  pinMode(sled , OUTPUT);
  pinMode(kled , OUTPUT);
  pinMode(buzzer , OUTPUT);
  pinMode(trig , OUTPUT);
  pinMode(echo , INPUT);
  Serial.begin(9600);
  Serial.println("KutluBilgi Akademi Yapimi");
}

void loop()
{
  digitalWrite(trig , HIGH);
  delayMicroseconds(1000);
  digitalWrite(trig , LOW);

  sure = pulseIn(echo , HIGH);
  mesafe = (sure / 2) / 29.1;

  if(mesafe >= 50)
  {
    digitalWrite(yled , HIGH);
       
  }
  else if(mesafe < 50 && mesafe > 20)
  {
    digitalWrite(sled , HIGH);
    digitalWrite(buzzer , HIGH);
    delay(50);
    digitalWrite(sled , LOW);   
    digitalWrite(buzzer , LOW);
    delay(500);
  }
  else if(mesafe < 20 && mesafe > 0)
  {
    digitalWrite(kled , HIGH);
    digitalWrite(buzzer , HIGH);
    delay(50);
    digitalWrite(kled , LOW);   
    digitalWrite(buzzer , LOW);
    delay(250);
  }
    
   
  Serial.print("Mesafe : ");
  Serial.print(mesafe);
  Serial.println(" cm");


  
  digitalWrite(yled , LOW);
  digitalWrite(sled , LOW);
  digitalWrite(kled , LOW);
}



